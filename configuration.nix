# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./hardware-acceleration.nix
       <nixpkgs/nixos/modules/profiles/hardened.nix>

    ];

  # Bootloader.
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/vda";
  boot.loader.grub.useOSProber = true;

  networking.hostName = "nixos-vm"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Australia/Melbourne";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_AU.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_AU.UTF-8";
    LC_IDENTIFICATION = "en_AU.UTF-8";
    LC_MEASUREMENT = "en_AU.UTF-8";
    LC_MONETARY = "en_AU.UTF-8";
    LC_NAME = "en_AU.UTF-8";
    LC_NUMERIC = "en_AU.UTF-8";
    LC_PAPER = "en_AU.UTF-8";
    LC_TELEPHONE = "en_AU.UTF-8";
    LC_TIME = "en_AU.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the KDE Plasma Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;

  # sddm is not 100% wayland yet and needs to fold into the kde project
  #services.xserver.displayManager.lightdm.enable = true;

  services.xserver.desktopManager.plasma5.enable = true;

  # default to wayland session
  #services.xserver.displayManager.defaultSession = "plasmawayland";
  # works with lightdm at the moment, sddm would drop the session into a tty

  # Configure keymap in X11
  services.xserver = {
    layout = "au";
    xkbVariant = "";
  };
  
  # enable fish as the default shell
    programs.fish.enable = true;
    users.defaultUserShell = pkgs.fish;

  # console tty look settings
    console = {
          packages=[ pkgs.terminus_font ];
          font="${pkgs.terminus_font}/share/consolefonts/ter-i22b.psf.gz";
          useXkbConfig = true; # use xkbOptions in tty.
  };

  # enable system to reboot faster
    systemd.extraConfig = "DefaultTimeoutStopSec=10s";

  # enable spice services for virt manager
    services.spice-vdagentd.enable = true;
    services.spice-webdavd.enable = true;
    services.qemuGuest.enable = true;

  # cleaning the nix store
  # automatic Garbage Collection
    nix.gc = {
                automatic = true;
                dates = "weekly";
                options = "--delete-older-than 7d";
             };

  # automatic upgrades
    system.autoUpgrade.enable = true;
    system.autoUpgrade.allowReboot = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
    system.copySystemConfiguration = true;

  # enable virt manager 
    programs.dconf.enable = true;
    # Manage the virtualisation services
      virtualisation = {
               libvirtd = {
               enable = true;
      qemu = {
        swtpm.enable = true;
        ovmf.enable = true;
        ovmf.packages = [ pkgs.OVMFFull.fd ];
      };
    };
    spiceUSBRedirection.enable = true;
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
    services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.changeme = {
    isNormalUser = true;
    description = "changeme";
    extraGroups = [ "networkmanager"
                    "wheel"
                    "libvirtd"
                    "kvm" 
                    "input" 
                    "disk"  ];
    packages = with pkgs; [
    #  firefox
    #  kate
    #  thunderbird
    ];
  };

  # Enable automatic login for the user.
  services.xserver.displayManager.autoLogin.enable = true;
  services.xserver.displayManager.autoLogin.user = "changeme";

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
     # base packages
     curl
     ffmpeg_6-full
     ffmpegthumbnailer
     glxinfo
     git
     gnupg
     htop
     mediainfo   
     neofetch
     nfs-utils
     nixos-option
     ntfs3g
     stdenv
     tailscale
     vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
     wayland-utils
     wget
     yt-dlp

     # rust cli tools
     atuin
     bat 
     exa
     lsd 
     ouch
     procs
     ripgrep
     sd
     tealdeer

     # spellcheck dictionaries
     aspell
     aspellDicts.en
     aspellDicts.en-computers
     aspellDicts.en-science
     hunspell
     hunspellDicts.en_AU-large 
     hunspellDicts.en_GB-large

     # archive tools
     atool
     bzip2
     gzip
     libarchive
     lz4
     lzip
     lzo
     lzop
     p7zip
     rzip
     unrar
     unzip
     xz
     zip
     zstd

     # gui apps
     kate
     #firefox

     # Development
     meld

     # Graphics
     blender
     
     digikam
     marble

     darktable
     gImageReader
     gimp
     inkscape
     krita
     
     # Internet
     brave
     #dialect
     filezilla
     lagrange
     
     # xmr
     monero-gui
     monero-cli
     xmrig
     p2pool
     trezor-suite
     #exodus
     # not working at the moment
     
     #tor
     #onionshare-gui
     #onionshare
     qbittorrent
     #tor-browser-bundle-bin

     # Multimedia
     #ardour
     handbrake
     libsForQt5.kamoso

     #kdenlive
     # apps for kdenlive
     #mediainfo
     glaxnimate

     obs-studio
     strawberry
     tenacity
     vlc

     # Office
     libreoffice-qt
     #onlyoffice-bin
     pdfarranger
     #standardnotes
     xournalpp

     # System 
     virt-manager
     libverto
     qemu_kvm
     qemu_full
     ventoy-full
     win-virtio
     virt-viewer
     spice
     spice-gtk
     spice-protocol
     win-spice
     gnome.adwaita-icon-theme
     etcher
     
     # Utilities
     #appflowy
     cryptomator
     kcalc
     livecaptions
     nextcloud-client
     vorta
     pika-backup

     # test os security
     lynis
     chkrootkit
     ossec
     crowdsec

     # dvd drives - yes, I still use them
     libdvdcss
     k3b

  ];

# setup firefox
programs = {
    firefox = {
      enable = true;
      policies = {
        CaptivePortal = false;
        DisableFirefoxAccounts = true;
        DisableFirefoxStudies = true;
        DisablePocket = true;
        DisableTelemetry = true;
        FirefoxHome = {
          Pocket = false;
          Snippets = false;
        };
        UserMessaging = {
          ExtensionRecommendations = false;
          SkipOnboarding = true;
        };
      };
    };
  };

# enable xdg desktop integration
xdg = {
portal = {
  enable = true;
  extraPortals = with pkgs; [
    xdg-desktop-portal-kde
    xdg-desktop-portal-wlr
    xdg-desktop-portal-gtk
  ];
 };
};

# set environment variables to hint Firefox to use Wayland features
environment.sessionVariables = {
   MOZ_ENABLE_WAYLAND = "1";
};

# exclude some kde apps
  environment.plasma5.excludePackages = with pkgs.libsForQt5; [
       elisa 
       plasma-browser-integration
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;boot.tmp.cleanOnBoot

  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  #  services.openssh.enable = true;
    services.openssh = {
      enable = true;
      settings.LogLevel = "VERBOSE";
      banner = "

#################################################################
#                   _    _           _   _                      #
#                  / \  | | ___ _ __| |_| |                     #
#                 / _ \ | |/ _ \ '__| __| |                     #
#                / ___ \| |  __/ |  | |_|_|                     #
#               /_/   \_\_|\___|_|   \__(_)                     #
#                                                               #
#  You are entering into a secured area! Your IP, Login Time,   #
#   Username has been noted and has been sent to the server     #
#                       administrator!                          #
#   This service is restricted to authorized users only. All    #
#            activities on this system are logged.              #
#  Unauthorized access will be fully investigated and reported  #
#        to the appropriate law enforcement agencies.           #
#################################################################

Welcome Back Legend !

NixOS home lab

";
        extraConfig = "

        AllowTcpForwarding no

        ClientAliveCountMax 2

        Compression no

        MaxAuthTries 3

        MaxSessions 2

        TCPKeepAlive no
        
        AllowAgentForwarding no

        ";

};



  # enable tailscale
    services.tailscale.enable = true;

  # enable trezor hardware wallet
    services.trezord.enable = true;
    
  # enable firewall and block all ports
    networking.firewall.enable = true;
    networking.firewall.allowedTCPPorts = [];
    networking.firewall.allowedUDPPorts = [];

  # Tell the firewall to implicitly trust packets routed over Tailscale:
    networking.firewall.trustedInterfaces = [ "tailscale0" ];

  # security improvements
    services.sysstat.enable = true;

  # disable coredump that could be exploited later
  # and also slow down the system when something crash
    systemd.coredump.enable = false;

  # enable firejail
  #  programs.firejail.enable = true;
  # more trouble than its worth at the moment

  # create system-wide executables firefox and chromium
  # that will wrap the real binaries so everything
  # work out of the box.
  #  programs.firejail.wrappedBinaries = {
  #    firefox = {
  #        executable = "${pkgs.lib.getBin pkgs.firefox}/bin/firefox";
  #        profile = "${pkgs.firejail}/etc/firejail/firefox.profile";
  #    };
  #     chromium = {
  #        executable = "${pkgs.lib.getBin pkgs.chromium}/bin/chromium";
  #        profile = "${pkgs.firejail}/etc/firejail/chromium.profile";
  #    };
  #  };

  # enable auditd
    security.auditd.enable = true;
    security.audit.enable = true;
    security.audit.rules = [
        "-a exit,always -F arch=b64 -S execve"
    ];

  # enable antivirus clamav and
  # keep the signatures' database updated
  # services.clamav.daemon.enable = true;
  # services.clamav.updater.enable = true;
  # does not start properly at the moment

  # enable open snitch
  #  services.opensnitch.enable = true;
  # does not start properly at the moment

  # enable out of memory stuff
    services.earlyoom = {
      enable = true;
      freeSwapThreshold = 2;
      freeMemThreshold = 2;
      extraArgs = [
          "-g" "--avoid '^(X|plasma.*|konsole|kwin)$'"
          "--prefer '^(electron|libreoffice|gimp)$'"
      ];
    };

  # clean up system and uses the latest mainline linux kernel
  # clean /tmp at boot
  #  boot.cleanTmpDir = true;
  # old code
    boot.tmp.cleanOnBoot = true;

  # latest kernel
    boot.kernelPackages = pkgs.linuxPackages_latest;

  # sync disk when buffer reach 6% of memory
    boot.kernel.sysctl = {
      "vm.dirty_ratio" = 6;
    };

  # clean out logs 
  # clean logs older than 2d
    services.cron.systemCronJobs = [
        "0 20 * * * root journalctl --vacuum-time=2d"
    ];

  # enable laptop services
    services.acpid.enable = true;
    services.thermald.enable = true;
    services.fwupd.enable = true;
    services.vnstat.enable = true;

  # create a strong and complex password beforehand 
    security.sudo.wheelNeedsPassword = false;


  # enable fonts 
  fonts.fonts = with pkgs; [
      alegreya
      alice
      amiri
      caladea
      cantarell-fonts
      carlito
      comfortaa
      comic-neue
      corefonts
      fira
      fira-code
      font-awesome
      hackgen-font
      ibm-plex
      inter
      iosevka
      jetbrains-mono
      lato
      liberation-sans-narrow
      liberation_ttf
      libertinus
      material-icons
      mona-sans
      montserrat
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-cjk-serif
      noto-fonts-emoji
      overpass
      pecita
      recursive
      source-code-pro
      source-han-sans-japanese
      source-han-serif-japanese
      source-sans
      source-sans-pro
      source-serif
      source-serif-pro
      terminus_font
      terminus_font_ttf
];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

}
