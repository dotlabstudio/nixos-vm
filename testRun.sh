#!/usr/bin/env bash

# this script automates my test run for a new configuration.nix on 
# the virtual machine

echo "This script should only be use in a virtual machine - don't use a bare metal!"

rm -rf nixos-vm

git clone https://gitlab.com/dotlabstudio/nixos-vm.git

sudo cp nixos-vm/configuration.nix /etc/nixos

sudo nixos-rebuild switch

echo "Please reboot machine with the new configuration.nix"

