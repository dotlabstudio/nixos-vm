# Personal NIX OS Configuration for virt manager

This git project just holds my personal nix Configurations files for a virtual machine to test code line sections using
mainly the nixos wiki pages.

# Main configuration file

```
sudo vim /etc/nixos/configuration.nix
```

# Change username with vim
```
:%s/changeme/yourusername/g
```

in normal mode

The letter s stands for substitute.

The keyword % will target the entire file instead of the current line. 

The flag g means “global”: more than one occurrence is targeted. Without it, only the first occurrence in the file (or the in line) would be replaced.

## Editing the configuration.nix

Use kate to copy the bootloader and luks section from the configuration.nix installation into this configuration.nix repo.

Change the username and hostname.

Comment out software that is not needed and add software for the new machine.

## After editing configuration file

update system
```
sudo nix-channel --update
```
then

```
sudo nixos-rebuild switch
```

A reboot recommended 

## Nixos rolling

```
sudo nix-channel --list

sudo nix-channel --add https://channels.nixos.org/nixos-unstable nixos

sudo nixos-rebuild switch --upgrade

```

from https://nixos.org/manual/nixos/stable/#sec-upgrading


### Setup fish as the default shell

```
programs.fish.enable = true;
users.defaultUserShell = pkgs.fish;
```

from https://nixos.wiki/wiki/Fish

### Make console tty look nicer

```
console = {
    packages=[ pkgs.terminus_font ];
    font="${pkgs.terminus_font}/share/consolefonts/ter-i22b.psf.gz";
    useXkbConfig = true; # use xkbOptions in tty.
  };
```

### Enable system to reboot faster

```
systemd.extraConfig = "DefaultTimeoutStopSec=10s";
```

from https://github.com/ChrisTitusTech/nixos-titus

### Use KDE Plasma Wayland by default

Launch KDE in Wayland session

```
services.xserver.displayManager.defaultSession = "plasmawayland";
```

This code does not work at the moment with sddm as it drops the session into a tty instead. 
Use lightdm instead.

```
services.xserver.displayManager.lightdm.enable = true;
```


for completeness for kde plasma 
```
services.xserver.enable = true;
services.xserver.displayManager.sddm.enable = true;
services.xserver.desktopManager.plasma5.enable = true;
```

```
programs.dconf.enable = true;
```

Automatic login for the user:
```
services.xserver.displayManager.autoLogin.enable = true;
services.xserver.displayManager.autoLogin.user = "changeme";
```  

from https://nixos.wiki/wiki/KDE

### Enable virt manager guest os tools

```
services.spice-vdagentd.enable = true;
services.spice-webdavd.enable = true;
services.qemuGuest.enable = true;
```
## Clean up the nix store and enable automatic upgrades
  
### Automatic Garbage Collection
```
nix.gc = {
                automatic = true;
                dates = "weekly";
                options = "--delete-older-than 7d";
         };
```
### Automatic upgrades

```
system.autoUpgrade.enable = true;
system.autoUpgrade.allowReboot = false;
```

### Copy the NixOS configuration file and link it from the resulting system (/run/current-system/configuration.nix). This is useful in case the accidentally delete configuration.nix.
```
system.copySystemConfiguration = true;
```

### Enable Tailscale in the NixOS configuration, and apply it:
```
services.tailscale.enable = true;
```
from https://tailscale.com/download/linux/nixos

### For system76 laptops
```
# System76
hardware.system76.enableAll = true;
```
from https://support.system76.com/articles/system76-software

### For hardware wallets

```
services.trezord.enable = true;

hardware.digitalbitbox.enable = true;
programs.digitalbitbox.enable = true;
```
in pkgs:
```
trezor-suite
```

### Virt manager under NixOS

```
  virtualisation = {
    libvirtd = {
      enable = true;
      qemu = {
        swtpm.enable = true;
        ovmf.enable = true;
        ovmf.packages = [ pkgs.OVMFFull.fd ];
      };
    };
    spiceUSBRedirection.enable = true;
  };

  services.spice-vdagentd.enable = true;

programs.dconf.enable = true;

environment.systemPackages = with pkgs; [ virt-manager ];
```
extra pkgs:

```
libverto
qemu_kvm
qemu_full
ventoy-full
win-virtio
virt-viewer
spice
spice-gtk
spice-protocol
win-spice
gnome.adwaita-icon-theme
```
add user to the libvirtd group
```
{
  users.users.$USER.extraGroups = [ "libvirtd" ];
}
```

#### Virt Manager first time run

```
File (in the menu bar) -> Add connection

HyperVisor = QEMU/KVM
Autoconnect = checkmark

Connect
```
from https://nixos.wiki/wiki/Virt-manager and https://github.com/TechsupportOnHold/Nixos-VM/blob/main/vm.nix

### Setting up firefox

```
programs = {
    firefox = {
      enable = true;
      policies = {
        CaptivePortal = false;
        DisableFirefoxAccounts = true;
        DisableFirefoxStudies = true;
        DisablePocket = true;
        DisableTelemetry = true;
        FirefoxHome = {
          Pocket = false;
          Snippets = false;
        };
        UserMessaging = {
          ExtensionRecommendations = false;
          SkipOnboarding = true;
        };
      };
    };
  };
  ```

  ### Enable xdg desktop integration:

  ```
  xdg = {
  portal = {
    enable = true;
    extraPortals = with pkgs; [
      xdg-desktop-portal-kde
      xdg-desktop-portal-wlr
      xdg-desktop-portal-gtk
    ];
  };
};
```

### Set environment variables to hint Firefox to use Wayland features. E.g.:

```
environment.sessionVariables = {
   MOZ_ENABLE_WAYLAND = "1";
};
```

from https://nixos.wiki/wiki/Firefox and https://github.com/hervyqa/dotfire

### Enable fonts 

```
fonts.fonts = with pkgs; [
      alegreya
      alice
      amiri
      caladea
      cantarell-fonts
      carlito
      comfortaa
      comic-neue
      corefonts
      fira
      fira-code
      font-awesome
      hackgen-font
      ibm-plex
      inter
      iosevka
      jetbrains-mono
      lato
      liberation-sans-narrow
      liberation_ttf
      libertinus
      material-icons
      mona-sans
      montserrat
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-cjk-serif
      noto-fonts-emoji
      overpass
      pecita
      recursive
      source-code-pro
      source-han-sans-japanese
      source-han-serif-japanese
      source-sans
      source-sans-pro
      source-serif
      source-serif-pro
      terminus_font
      terminus_font_ttf
];
```

from https://nixos.wiki/wiki/Fonts#Flatpak_applications_can.27t_find_system_fonts

### Enable flatpak support

```
services.flatpak.enable = true;
services.dbus.enable = true;
```

from https://nixos.wiki/wiki/Flatpak

Add the Flathub repository
```
flatpak remote-add --user --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
```

from https://flathub.org/en-GB/setup/NixOS

### Secuirty Improvments

```
services.sysstat.enable = true;
```

in the pkgs section:
```
chkrootkit

ossec

crowdsec
```

### SSH Server

```
  services.openssh = {
      enable = true;
      settings.LogLevel = "VERBOSE";
      banner = "

#################################################################
#                   _    _           _   _                      #
#                  / \  | | ___ _ __| |_| |                     #
#                 / _ \ | |/ _ \ '__| __| |                     #
#                / ___ \| |  __/ |  | |_|_|                     #
#               /_/   \_\_|\___|_|   \__(_)                     #
#                                                               #
#  You are entering into a secured area! Your IP, Login Time,   #
#   Username has been noted and has been sent to the server     #
#                       administrator!                          #
#   This service is restricted to authorized users only. All    #
#            activities on this system are logged.              #
#  Unauthorized access will be fully investigated and reported  #
#        to the appropriate law enforcement agencies.           #
#################################################################

";
        extraConfig = "

        AllowTcpForwarding no

        ClientAliveCountMax 2

        Compression no

        MaxAuthTries 3

        MaxSessions 2

        TCPKeepAlive no
        
        AllowAgentForwarding no

        ";

};

```

from https://nixos.wiki/wiki/SSH_public_key_authentication

# Harden NixOS

```
imports =
   [
      ./hardware-configuration.nix
      <nixpkgs/nixos/modules/profiles/hardened.nix>
   ];

  # enable firewall and block all ports
  networking.firewall.enable = true;
  networking.firewall.allowedTCPPorts = [];
  networking.firewall.allowedUDPPorts = [];

  # disable coredump that could be exploited later
  # and also slow down the system when something crash
  systemd.coredump.enable = false;

  # required to run chromium
  security.chromiumSuidSandbox.enable = true;

  # enable firejail
  programs.firejail.enable = true;

  # create system-wide executables firefox and chromium
  # that will wrap the real binaries so everything
  # work out of the box.
  programs.firejail.wrappedBinaries = {
      firefox = {
          executable = "${pkgs.lib.getBin pkgs.firefox}/bin/firefox";
          profile = "${pkgs.firejail}/etc/firejail/firefox.profile";
      };
      chromium = {
          executable = "${pkgs.lib.getBin pkgs.chromium}/bin/chromium";
          profile = "${pkgs.firejail}/etc/firejail/chromium.profile";
      };
  };

  # enable antivirus clamav and
  # keep the signatures' database updated
  services.clamav.daemon.enable = true;
  services.clamav.updater.enable = true;
  ```

  from https://dataswamp.org/~solene/2022-01-13-nixos-hardened.html

  ### Open snitch

  ```
  services.opensnitch.enable = true;
  ```

  ### Auditd services
  ```
  security.auditd.enable = true;
  security.audit.enable = true;
  security.audit.rules = [
    "-a exit,always -F arch=b64 -S execve"
  ];
  ```

  from https://xeiaso.net/blog/paranoid-nixos-2021-07-18

# Extra Configurations

```
  services.earlyoom = {
      enable = true;
      freeSwapThreshold = 2;
      freeMemThreshold = 2;
      extraArgs = [
          "-g" "--avoid '^(X|plasma.*|konsole|kwin)$'"
          "--prefer '^(electron|libreoffice|gimp)$'"
      ];
  };

  # clean /tmp at boot
  # boot.cleanTmpDir = true;
  # above is old code narrow
  boot.tmp.cleanOnBoot = true;


  # latest kernel
  boot.kernelPackages = pkgs.linuxPackages_latest;

  # sync disk when buffer reach 6% of memory
  boot.kernel.sysctl = {
      "vm.dirty_ratio" = 6;
  };

  # clean logs older than 2d
  services.cron.systemCronJobs = [
      "0 20 * * * root journalctl --vacuum-time=2d"
  ];

  # services
  services.acpid.enable = true;
  services.thermald.enable = true;
  services.fwupd.enable = true;
  services.vnstat.enable = true;

  ```

  from https://dataswamp.org/~solene/2022-09-28-earlyoom.html and https://dataswamp.org/~solene/2021-12-21-my-nixos.html


